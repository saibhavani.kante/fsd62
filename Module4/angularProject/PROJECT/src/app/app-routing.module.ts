import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { HomePageComponent } from './home-page/home-page.component';
import { AboutusComponent } from './aboutus/aboutus.component';
import { LOGOUTComponent } from './logout/logout.component';

const routes: Routes = [
  {path:'',        component:LoginComponent},
  {path:'login',   component:LoginComponent},
  {path:'Register', component:RegisterComponent},
  {path:'homepage', component:HomePageComponent},
  {path:'aboutus',  component:AboutusComponent},
  {path:'logout',   component:LOGOUTComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
